console.log("Game dimulai");

const player1 = new HumanPlayer('PLAYER 1');
const player2 = new ComputerPlayer('COM');

player1.choose();
console.log(`${player1.name} memilih pilihan: ${player1.choice}`);

player2.choose();
console.log(`${player2.name} memilih pilihan: ${player2.choice}`);

const suitGame = new SuitGame(player1, player2);
console.log(`Hasilnya adalah ${suitGame.result}`);