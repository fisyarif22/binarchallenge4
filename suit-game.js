    class SuitGame {
    vs = 'VS';
    result;
    logo = '';
    background = '#9C835F';
    player1;
    player2;

    constructor(player1, player2) {
        this.player1 = player1;
        this.player2 = player2;

        this.determineTheWinner();
    }

    restart() {
        this.result = '';
        this.player1.restart();
        this.player2.restart();

        this.player1.choose();
        this.player2.choose();
        this.determineTheWinner();
    }

    back() {}

    determineTheWinner() {
        if (this.player1.choice === this.player2.choice) {
            this.result = "DRAW";
            return;
        }

        const player12Choice = this.player1.choice + this.player2.choice;
        switch (player12Choice) {
            case "SCISSORPAPER":
            case "PAPERROCK":
            case "ROCKSCISSOR": {
                this.result = `${this.player1.name} Win`;
                return;
            }
            case "PAPERSCISSOR":
            case "ROCKPAPER":
            case "SCISSORROCK": {
                this.result = `${this.player2.name} Win`;
                return;
            }
        }
    }
}